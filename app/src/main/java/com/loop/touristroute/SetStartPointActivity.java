package com.loop.touristroute;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Set;

public class SetStartPointActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener {

    GoogleMap map;
    MapFragment mapFragment;
    GoogleMapsHelper gmh;
    String startLocationCoordinates = "null";
    String notSavedBounds = "null";
    boolean isCoordinatesChanged = false;
    LatLng markerLatLng = null;
    int padding = 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_start_point);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            startLocationCoordinates = bundle.getString("start_location_coordinates");
            if (startLocationCoordinates != null) {
                if (!startLocationCoordinates.equals("null")) {
                    ParserHelper ph = new ParserHelper();
                    markerLatLng = ph.StringToLatLng(startLocationCoordinates);
                }
            }
            notSavedBounds = bundle.getString("lat_lng_bounds");
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        gmh = new GoogleMapsHelper(this, map);
        gmh.setBounds(notSavedBounds);
        gmh.viewAllGeoObjects(-1);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMarkerDragListener(this);
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (markerLatLng == null) {
                    map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("Start")
                    .draggable(true));
                    isCoordinatesChanged = true;
                    markerLatLng = latLng;
                }
            }
        });

        if (markerLatLng != null) {
            map.addMarker(new MarkerOptions()
            .position(markerLatLng)
            .title("Start")
            .draggable(true));
        }
    }

    public void btnClickSave(View view) {
        Bundle bundle = new Bundle();
        if (markerLatLng != null) {
            ParserHelper ph = new ParserHelper();
            bundle.putString("start_location_coordinates", ph.LatLngToString(markerLatLng));
        } else {
            bundle.putString("start_location_coordinates", "null");
        }
        bundle.putBoolean("is_coordinates_changed", isCoordinatesChanged);
        Intent intent = new Intent(SetStartPointActivity.this, CreateRouteActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    public void btnClickCancel(View view) {
        Bundle bundle = new Bundle();
        if (!startLocationCoordinates.equals("null")) {
            bundle.putString("start_location_coordinates", startLocationCoordinates);
        } else {
            bundle.putString("start_location_coordinates", "null");
        }
        bundle.putBoolean("is_coordinates_changed", false);
        Intent intent = new Intent(SetStartPointActivity.this, CreateRouteActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        isCoordinatesChanged = true;
        markerLatLng = marker.getPosition();
    }

}
