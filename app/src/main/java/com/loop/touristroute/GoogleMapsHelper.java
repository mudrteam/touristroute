package com.loop.touristroute;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapsHelper {

    private Context context;
    private GoogleMap map;

    int padding = 60;

    GoogleMapsHelper(Context context, GoogleMap map) {
        this.context = context;
        this.map = map;
    }

    //              notDisplayedGeoObjectId = -1 for display all geo objects
    public void viewAllGeoObjects(long notDisplayedGeoObjectId) {
        DBHelper dbHelper = new DBHelper(context);
        Cursor c = dbHelper.getTheseColumns(true, true, true, true, false, false, false);
        if (c.moveToFirst()) {
            long id;
            String name;
            String type;
            String coordinates;
            ParserHelper ph = new ParserHelper();
            do {
                id = c.getLong(c.getColumnIndex("geo_object_id"));
                if (id != notDisplayedGeoObjectId) {
                    if ((name = c.getString(c.getColumnIndex("geo_object_name"))) == null) {
                        name = "";
                    }
                    if ((type = c.getString(c.getColumnIndex("geo_object_type"))) == null) {
                        type = "0";
                    }
                    if ((coordinates = c.getString(c.getColumnIndex("geo_object_coordinates"))) == null) {
                        coordinates = "null";
                    }
                    Marker m = map.addMarker(new MarkerOptions()
                            .position(ph.StringToLatLng(coordinates))
                            .title(name)
                    );
                    setMarkerIcon(m, Integer.parseInt(type));
                }
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
    }

    public void setMarkerIcon(Marker m, int type) {
        switch (type) {
            case 0:
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_eatery));
                break;
            case 1:
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_museum));
                break;
            case 2:
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_attraction));
                break;
            case 3:
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_amusement));
                break;
            case 4:
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_hotel));
                break;
        }
    }

    public void setBounds(String notSavedBounds) {
        ParserHelper ph = new ParserHelper();
        LatLngBounds bounds = null;
        if (notSavedBounds.equals("null")) {
            final String APP_PREFERENCES = "app_settings";
            final String GEO_OBJECTS_BOUNDS = "geo_objects_bounds";
            SharedPreferences appSettings;
            appSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            String stringBounds = appSettings.getString(GEO_OBJECTS_BOUNDS, "");
            if (!stringBounds.equals("")) {
                bounds = ph.StringToLatLngBounds(stringBounds);
            }
        }
        else {
            bounds = ph.StringToLatLngBounds(notSavedBounds);
        }
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.moveCamera(cu);
    }

}
