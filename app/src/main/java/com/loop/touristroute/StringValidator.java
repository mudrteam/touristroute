package com.loop.touristroute;

import android.util.Patterns;

public class StringValidator {

    public boolean isEmptyString(String string) {
        return string.isEmpty();
    }

    public String removeWhitespaces(String string) {
        return string.trim();
    }

    public boolean isValidURL(String stringURL) {
        return Patterns.WEB_URL.matcher(stringURL).matches();
    }

    /*
    public boolean isURLResponse(String stringURL) {
        connectionTask ct = new connectionTask();
        try {
            URL url = new URL(stringURL);
            ct.execute(url);
            try {
                boolean result = ct.get();
                return result;
            } catch (ExecutionException e) {
                return false;
            } catch (InterruptedException e) {
                return false;
            }
        } catch (MalformedURLException e) {
            return false;
        }
    }

    private class connectionTask extends AsyncTask<URL, Boolean, Boolean> {
        protected Boolean doInBackground(URL... urls) {
            try {
                URLConnection conn = urls[0].openConnection();
                conn.connect();
            } catch (IOException e) {
                return false;
            }
            return true;
        }

    }
    */
}
