package com.loop.touristroute;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

public class ParserHelper {

    public LatLng StringToLatLng(String latLng) {
        String[] latlng =  latLng.split(",");
        double latitude = Double.parseDouble(removeFirstChars(latlng[0], 10));
        double longitude = Double.parseDouble(removeLastChar(latlng[1]));
        return new LatLng(latitude, longitude);
    }

    public LatLng StringWithoutBracketsToLatLng(String latLng) {
        String[] latlng =  latLng.split(",");
        double latitude = Double.parseDouble(latlng[0]);
        double longitude = Double.parseDouble(latlng[1]);
        return new LatLng(latitude, longitude);
    }

    public String getLatLngForUrlRequest(String LatLng){
        int startIndex = LatLng.indexOf("(");
        int endIndex = LatLng.indexOf(")");
        return LatLng.substring(startIndex + 1, endIndex);
    }

    /*
    public double getDoubleLatFromLatLng(String LatLng) {
        int startIndex = LatLng.indexOf("(");
        int endIndex = LatLng.indexOf(",");
        return Double.parseDouble(LatLng.substring(startIndex + 1, endIndex));
    }

    public double getDoubleLngFromLatLng(String LatLng) {
        int startIndex = LatLng.indexOf(",");
        int endIndex = LatLng.indexOf(")");
        return Double.parseDouble(LatLng.substring(startIndex + 1, endIndex));
    }
    */
    public PolylineOptions getPolylineOptionsFromString(String polylinePoints) {
        PolylineOptions polylineOptions = new PolylineOptions();
        int lastIndexOf = polylinePoints.lastIndexOf("(");
        int startIndex;
        int endIndex = 0;
        do {
            startIndex = polylinePoints.indexOf("(", endIndex);
            endIndex = polylinePoints.indexOf(")", startIndex);
            polylineOptions.add(StringWithoutBracketsToLatLng(polylinePoints.substring(startIndex + 1, endIndex)));
        } while (startIndex < lastIndexOf);
        return polylineOptions;
    }


    public LatLng getSWFromBounds(String latLngBounds) {
        int swStartIndex = latLngBounds.indexOf("(");
        int swEndIndex = latLngBounds.indexOf(")", swStartIndex);
        String sw = latLngBounds.substring(swStartIndex + 1, swEndIndex);
        String[] stringSW = sw.split(",");
        return new LatLng(Double.parseDouble(stringSW[0]), Double.parseDouble(stringSW[1]));
    }

    public LatLng getNEFromBounds(String latLngBounds) {
        int swStartIndex = latLngBounds.indexOf("(");
        int neStartIndex = latLngBounds.indexOf("(", swStartIndex+1);
        int neEndIndex = latLngBounds.indexOf(")", neStartIndex);
        String ne = latLngBounds.substring(neStartIndex + 1, neEndIndex);
        String[] stringNE = ne.split(",");
        return new LatLng(Double.parseDouble(stringNE[0]), Double.parseDouble(stringNE[1]));
    }

    public LatLngBounds StringToLatLngBounds(String latLngBounds) {
        LatLng latLngSW = getSWFromBounds(latLngBounds);
        LatLng latLngNE = getNEFromBounds(latLngBounds);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(latLngSW);
        builder.include(latLngNE);
        return builder.build();
    }

    public String LatLngToString(LatLng latLng) {
        return String.valueOf(latLng);
    }

    public String removeFirstChars(String string, int numOfRemovedChars) {
        return string.substring(numOfRemovedChars);
    }

    public String removeLastChar(String string) {
        return string.substring(0,string.length()-1);
    }

    public String twoNumbersToTime(String hours, String minutes) {
        int min;
        if (minutes.equals("")) {
            min = 0;
        }
        else {
            min = Integer.parseInt(minutes);
        }
        String formattedMinutes = minutes;
        if (min > 59) {
            formattedMinutes = "59";
        }
        else if (min < 10) {
            formattedMinutes = "0" + String.valueOf(min);
        }
        int hr;
        if (hours.equals("")) {
            hr = 0;
        }
        else {
            hr = Integer.parseInt(hours);
        }
        String formattedHours = hours;
        if (hr > 99) {
            formattedHours = "99";
        }
        else if (hr < 10) {
            formattedHours = "0" + String.valueOf(hr);
        }
        return formattedHours + ":" + formattedMinutes;
    }

    public String getHoursFromTime(String time) {
        if (time.length() == 5) {
            return time.substring(0, 2);
        }
        return "";
    }

    public String getMinutesFromTime(String time) {
        if (time.length() == 5) {
            return time.substring(3, 5);
        }
        return "";
    }

}
