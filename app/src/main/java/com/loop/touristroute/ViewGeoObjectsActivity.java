package com.loop.touristroute;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.concurrent.ExecutionException;

public class ViewGeoObjectsActivity extends AppCompatActivity implements OnClickListener {

    String[] sortBy = {"types ascending", "types descending", "names ascending", "names descending",
            "create time ascending", "create time descending", "time ascending", "time descending"};
    Spinner spinnerSortBy;
    int columnNumber = 0;
    boolean isAscending = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_geo_objects);
        createSpinner();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean isCoordinatesChanged = bundle.getBoolean("is_coordinates_changed");
            if (isCoordinatesChanged) {
                ThreadsClass th = new ThreadsClass(this);
                th.calculateBounds();
            }
        }
    }

    protected void createSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.spinner_item_layout, sortBy);
        spinnerSortBy = (Spinner) findViewById(R.id.spinnerSortBy);
            spinnerSortBy.setAdapter(adapter);
            spinnerSortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            columnNumber = 3;
                            isAscending = true;
                            break;
                        case 1:
                            columnNumber = 3;
                            isAscending = false;
                            break;
                        case 2:
                            columnNumber = 1;
                            isAscending = true;
                            break;
                        case 3:
                            columnNumber = 1;
                            isAscending = false;
                            break;
                        case 4:
                            columnNumber = 0;
                            isAscending = true;
                            break;
                        case 5:
                            columnNumber = 0;
                            isAscending = false;
                            break;
                        case 6:
                            columnNumber = 6;
                            isAscending = true;
                            break;
                        case 7:
                            columnNumber = 6;
                            isAscending = false;
                            break;
                        default:
                            columnNumber = 0;
                            isAscending = true;
                            break;
                    }
                    LinearLayout layout_geo_objects = (LinearLayout)findViewById(R.id.geoObjectsLayout);
                    layout_geo_objects.removeAllViews();
                    try {
                        AsyncTaskParameters params = new AsyncTaskParameters(columnNumber, isAscending);
                        createGeoObjectsLayout createLayout = new createGeoObjectsLayout();
                        LinearLayout geoObjectsLayout = createLayout.execute(params).get();
                        if (geoObjectsLayout!= null) {
                            layout_geo_objects.addView(geoObjectsLayout);
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });
    }

    protected LinearLayout viewAllGeoObjects (int colNum, boolean isAsc) {
        LinearLayout returnedGeoObjectsLayout = new LinearLayout(this);
        returnedGeoObjectsLayout.setOrientation(LinearLayout.VERTICAL);
            DBHelper dbHelper = new DBHelper(this);
            Cursor c = dbHelper.getAllRows(colNum, isAsc);
            if (c.moveToFirst()) {
                String name;
                String type;
                String title;
                String link;
                String time;
                boolean haveTitle;
                boolean haveLink;
                boolean haveTime;
                do {
                    haveTitle = false;
                    haveLink = false;
                    haveTime = false;
                    int id = c.getInt(c.getColumnIndex("geo_object_id"));
                    if ((name = c.getString(c.getColumnIndex("geo_object_name"))) == null) {
                        name = "";
                    }
                    if ((type = c.getString(c.getColumnIndex("geo_object_type"))) == null) {
                        type = "0";
                    }
                    title = c.getString(c.getColumnIndex("geo_object_title"));
                    if (title != null) {
                        if (!title.equals("")) {
                            haveTitle = true;
                        }
                    }
                    link = c.getString(c.getColumnIndex("geo_object_link"));
                    if (link != null) {
                        if (!link.equals("")) {
                            haveLink = true;
                        }
                    }
                    time = c.getString(c.getColumnIndex("geo_object_time"));
                    if (time != null) {
                        if (!time.equals("00:00")) {
                            haveTime = true;
                        }
                    }
                    returnedGeoObjectsLayout.addView(createGeoObjectLayout(id, name, Integer.parseInt(type),
                            haveTitle, haveLink, haveTime));
                } while (c.moveToNext());
            }
        c.close();
        dbHelper.close();
        return returnedGeoObjectsLayout;
    }

    private LinearLayout createGeoObjectLayout(int id, String name, int type, boolean haveTitle,
                                               boolean haveLink, boolean haveTime) {
        LayoutHelper lh = new LayoutHelper();
        LinearLayout layoutGeoObject = new LinearLayout(this);
        layoutGeoObject.setLayoutParams(lh.getLayoutGeoObjectParams());
        //              button
        Button btnGeoObject = new Button(this);
        btnGeoObject.setId(id);
        btnGeoObject.setText(name);
        btnGeoObject.setOnClickListener(this);
        btnGeoObject.setLayoutParams(lh.getBtnViewGeoObjectParams());
        //              image
        ImageView imgGeoObjectType = new ImageView(this);
        switch (type){
            case 0:
                imgGeoObjectType.setImageResource(R.drawable.img_eatery);
                break;
            case 1:
                imgGeoObjectType.setImageResource(R.drawable.img_museum);
                break;
            case 2:
                imgGeoObjectType.setImageResource(R.drawable.img_attraction);
                break;
            case 3:
                imgGeoObjectType.setImageResource(R.drawable.img_amusement);
                break;
            case 4:
                imgGeoObjectType.setImageResource(R.drawable.img_hotel);
                break;
        }
        imgGeoObjectType.setLayoutParams(lh.getImgViewTypeParams());
        //              geo object parameters layout
        LinearLayout layoutParameters = new LinearLayout(this);
        layoutParameters.setLayoutParams(lh.getLayoutGeoObjectParamsParams());
        if (haveTitle) {
            ImageView imgTitle = new ImageView(this);
            imgTitle.setImageResource(R.drawable.img_title);
            imgTitle.setLayoutParams(lh.getImgViewGeoObjectParams());
            layoutParameters.addView(imgTitle);
        }
        if (haveLink) {
            ImageView imgLink = new ImageView(this);
            imgLink.setImageResource(R.drawable.img_link);
            imgLink.setLayoutParams(lh.getImgViewGeoObjectParams());
            layoutParameters.addView(imgLink);
        }
        if (haveTime) {
            ImageView imgTime = new ImageView(this);
            imgTime.setImageResource(R.drawable.img_time);
            imgTime.setLayoutParams(lh.getImgViewGeoObjectParams());
            layoutParameters.addView(imgTime);
        }
        //              geo object layout
        layoutGeoObject.setOrientation(LinearLayout.HORIZONTAL);
        layoutGeoObject.addView(imgGeoObjectType);
        layoutGeoObject.addView(btnGeoObject);
        layoutGeoObject.addView(layoutParameters);
        return layoutGeoObject;
    }

    public void clickBtnBack(View view) {
        Intent intent = new Intent(ViewGeoObjectsActivity.this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    public void clickBtnAddGeoObject(View view) {
        Bundle bundle = new Bundle();
        bundle.putLong("geo_object_id", -1);
        Intent intent = new Intent(ViewGeoObjectsActivity.this, EditGeoObjectParametersActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        final int clicked_id = view.getId();
        DBHelper dbHelper = new DBHelper(this);
        ContentValues cv = dbHelper.getAllColsInRow(clicked_id);
        dbHelper.close();
        String name = cv.getAsString("name");
        String coordinates = cv.getAsString("coordinates");
        int type = cv.getAsInteger("type");
        String title = cv.getAsString("title");
        String link = cv.getAsString("link");
        String time = cv.getAsString("time");
        cv.clear();
        Bundle bundle = new Bundle();
        bundle.putString("geo_object_coordinates", coordinates);
        bundle.putString("geo_object_name", name);
        bundle.putString("geo_object_title", title);
        bundle.putString("geo_object_link", link);
        bundle.putString("geo_object_time", time);
        bundle.putInt("geo_object_type", type);
        bundle.putLong("geo_object_id", clicked_id);
        Intent intent = new Intent(ViewGeoObjectsActivity.this, EditGeoObjectParametersActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }


    private class createGeoObjectsLayout extends AsyncTask <AsyncTaskParameters, Void, LinearLayout> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected LinearLayout doInBackground(AsyncTaskParameters... params) {
            int colNum = params[0].colNum;
            boolean isAsc = params[0].isAsc;
            return viewAllGeoObjects(colNum, isAsc);
        }

        @Override
        protected void onPostExecute(LinearLayout linearLayout) {
            super.onPostExecute(linearLayout);
        }
    }

    private static class AsyncTaskParameters {
        int colNum;
        boolean isAsc;

        AsyncTaskParameters(int colNum, boolean isAsc) {
            this.colNum = colNum;
            this.isAsc = isAsc;
        }
    }
}

