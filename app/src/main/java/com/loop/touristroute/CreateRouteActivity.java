package com.loop.touristroute;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class CreateRouteActivity extends AppCompatActivity {

    CheckBox checkboxHotel;
    CheckBox checkboxEatery;
    CheckBox checkboxAmusement;
    CheckBox checkboxMuseum;
    CheckBox checkboxAttraction;

    boolean isNeedHotel;
    boolean isNeedEatery;
    boolean isNeedAmusement;
    boolean isNeedMuseum;
    boolean isNeedAttraction;

    String startLocationCoordinates = "null";
    String notSavedBounds = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_route);
        checkboxHotel = (CheckBox) findViewById(R.id.checkboxHotel);
        checkboxEatery = (CheckBox) findViewById(R.id.checkboxEatery);
        checkboxAmusement = (CheckBox) findViewById(R.id.checkboxAmusement);
        checkboxMuseum = (CheckBox) findViewById(R.id.checkboxMuseum);
        checkboxAttraction = (CheckBox) findViewById(R.id.checkboxAttraction);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            startLocationCoordinates = bundle.getString("start_location_coordinates");
            boolean isCoordinatesChanged = bundle.getBoolean("is_coordinates_changed");
            if (isCoordinatesChanged) {
                if (!startLocationCoordinates.equals("null")) {
                    ThreadsClass th = new ThreadsClass(this);
                    notSavedBounds = String.valueOf(th.calculateBounds(startLocationCoordinates));
                }
            }
        }
    }

    public void clickBtnCancel(View view) {
        Intent intent = new Intent(CreateRouteActivity.this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    public void clickBtnCreate(View view) {
        ArrayList<String> pointsList;
        pointsList = createRoute();
        PolylineOptions polylineOptions = createPolyline(pointsList);
        Bundle bundle = new Bundle();
        bundle.putString("polyline_options", polylineOptions.getPoints().toString());
        Intent intent = new Intent(CreateRouteActivity.this, ViewRouteOnMapActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
        /*
        Intent intent = new Intent(CreateRouteActivity.this, MainMenuActivity.class);
        startActivity(intent);
        finish();
        */
    }

    public void btnClickSetStartPoint(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("start_location_coordinates", startLocationCoordinates);
        bundle.putString("lat_lng_bounds", notSavedBounds);
        Intent intent = new Intent(CreateRouteActivity.this, SetStartPointActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void readCheckBoxValues() {
        isNeedHotel = checkboxHotel.isChecked();
        isNeedEatery = checkboxEatery.isChecked();
        isNeedAmusement = checkboxAmusement.isChecked();
        isNeedMuseum = checkboxMuseum.isChecked();
        isNeedAttraction = checkboxAttraction.isChecked();
    }

    private PolylineOptions createPolyline(ArrayList<String> coordinatesList) {
        PolylineOptions polylineOptions = new PolylineOptions();
        ArrayList<LatLng> pointsList = new ArrayList<>();
        int numOfCoordinates = coordinatesList.size();
        if (numOfCoordinates > 1) {
            for (int i = 0; i < numOfCoordinates - 1; i++) {
                try {
                    GoogleMapsApiConnection gmac = new GoogleMapsApiConnection(coordinatesList.get(i), coordinatesList.get(i + 1));
                    pointsList.addAll(gmac.getLatLngArrayList());
                } catch (ExecutionException | InterruptedException | JSONException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 0; i < pointsList.size(); i++) {
                polylineOptions.add(pointsList.get(i));
            }
        }
        return polylineOptions;
    }

    private ArrayList<String> createRoute() {
        ArrayList<String> coordinatesList = new ArrayList<>();
        readCheckBoxValues();
        String originCoordinates = startLocationCoordinates;
        coordinatesList.add(originCoordinates);
        String destinationCoordinates;
        if (isNeedMuseum) {
            destinationCoordinates = findClosestObjectByType(originCoordinates, 1);
            if (destinationCoordinates != null) {
                coordinatesList.add(destinationCoordinates);
                originCoordinates = destinationCoordinates;
            }
        }
        if (isNeedAttraction) {
            destinationCoordinates = findClosestObjectByType(originCoordinates, 2);
            if (destinationCoordinates != null) {
                coordinatesList.add(destinationCoordinates);
                originCoordinates = destinationCoordinates;
            }
        }
        if (isNeedAmusement) {
            destinationCoordinates = findClosestObjectByType(originCoordinates, 3);
            if (destinationCoordinates != null) {
                coordinatesList.add(destinationCoordinates);
                originCoordinates = destinationCoordinates;
            }
        }
        if (isNeedEatery) {
            destinationCoordinates = findClosestObjectByType(originCoordinates, 0);
            if (destinationCoordinates != null) {
                coordinatesList.add(destinationCoordinates);
                originCoordinates = destinationCoordinates;
            }
        }
        if (isNeedHotel) {
            destinationCoordinates = findClosestObjectByType(originCoordinates, 4);
            if (destinationCoordinates != null) {
                coordinatesList.add(destinationCoordinates);
            }
        }
        return coordinatesList;
    }

    private String findClosestObjectByType(String originCoordinates, int typeId) {
        GoogleMapsApiConnection gmh;
        DBHelper dbHelper = new DBHelper(this);
        Cursor c = dbHelper.getGeoObjectByType(typeId);
        String destinationCoordinates;
        String nearestGeoObjectCoordinates = null;
        int minDuration = -1;
        int currentDuration;
        long nearestGeoObjectId = -1;
        if (c.moveToFirst()) {
            do {
                long id = c.getInt(c.getColumnIndex("geo_object_id"));
                if ((destinationCoordinates = c.getString(c.getColumnIndex("geo_object_coordinates"))) != null) {
                    try {
                        gmh = new GoogleMapsApiConnection(originCoordinates, destinationCoordinates);
                        currentDuration = gmh.getDuration();
                        if (minDuration == -1) {
                            minDuration = currentDuration;
                            nearestGeoObjectId = id;
                            nearestGeoObjectCoordinates = destinationCoordinates;
                        } else {
                            if (minDuration > currentDuration) {
                                minDuration = currentDuration;
                                nearestGeoObjectId = id;
                                nearestGeoObjectCoordinates = destinationCoordinates;
                            }
                        }
                        gmh.getDuration();
                    } catch (ExecutionException |InterruptedException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            } while (c.moveToNext());
        }
        c.close();
        dbHelper.close();
        return nearestGeoObjectCoordinates;
    }

}
