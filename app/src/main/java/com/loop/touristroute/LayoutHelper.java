package com.loop.touristroute;

import android.view.ViewGroup;
import android.widget.LinearLayout;

public class LayoutHelper {

    public LinearLayout.LayoutParams getLayoutGeoObjectParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 30);
        return layoutParams;
    }

    public LinearLayout.LayoutParams getBtnViewGeoObjectParams() {
        return new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
    }

    public LinearLayout.LayoutParams getImgViewGeoObjectParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        layoutParams.setMargins(0, 20, 0, 20);
        return layoutParams;
    }

    public LinearLayout.LayoutParams getImgViewTypeParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3);
        layoutParams.setMargins(15, 15, 15, 15);
        return layoutParams;
    }

    public LinearLayout.LayoutParams getLayoutGeoObjectParamsParams() {
        return new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3);
    }

    public LinearLayout.LayoutParams getControlBtnViewParams() {
        return new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
    }

}
