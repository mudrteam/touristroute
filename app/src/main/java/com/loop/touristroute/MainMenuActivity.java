package com.loop.touristroute;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.util.Log;

import java.util.concurrent.ExecutionException;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_menu);
    }

    public void clickBtnMyRoutes(View view) throws ExecutionException, InterruptedException {
        DBHelper dbHelper = new DBHelper(this);
        Cursor c = dbHelper.getGeoObjectByType(1);
        String coordinates;
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex("geo_object_id"));
                if ((coordinates = c.getString(c.getColumnIndex("geo_object_coordinates"))) == null) {
                    coordinates = "";
                }
                System.out.println("id = " + id);
                System.out.println("coordinates = " + coordinates);
            } while (c.moveToNext());
        }
    }

    public void clickBtnDB(View view) {
        final String APP_PREFERENCES = "app_settings";
        final String GEO_OBJECTS_BOUNDS = "geo_objects_bounds";
        SharedPreferences sp;
        sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String latLngBounds = sp.getString(GEO_OBJECTS_BOUNDS, "");
        Log.v("bounds from prefer =", latLngBounds);
    }

    public void clickBtnReadDB(View view)  {
        //////////////      getAllRows      ////////////////
        ParserHelper ph = new ParserHelper();
        DBHelper dbHelper = new DBHelper(this);
        Cursor c = dbHelper.getAllRows();
        if (c.moveToFirst()) {
            String name;
            String coordinates;
            String type;
            String title;
            String link;
            String time;
            do {
                int id = c.getInt(c.getColumnIndex("geo_object_id"));
                if ((name = c.getString(c.getColumnIndex("geo_object_name"))) == null) {
                    name = "";
                }
                if ((coordinates = c.getString(c.getColumnIndex("geo_object_coordinates"))) == null) {
                    coordinates = "";
                }
                if ((type = c.getString(c.getColumnIndex("geo_object_type"))) == null) {
                    type = "";
                }
                if ((title = c.getString(c.getColumnIndex("geo_object_title"))) == null) {
                    title = "";
                }
                if ((link = c.getString(c.getColumnIndex("geo_object_link"))) == null) {
                    link = "";
                }
                if ((time = c.getString(c.getColumnIndex("geo_object_time"))) == null) {
                    time = "";
                }
                Log.v("--------", "-----------");
                Log.v("id = ", Integer.toString(id));
                Log.v("name = ", name);
                Log.v("coordinates = ", coordinates);
                Log.v("type = ", type);
                Log.v("title = ", title);
                Log.v("link = ", link);
                Log.v("time = ", time);
            } while (c.moveToNext());
        }
        c.close();

    }

    public void clickBtnAddGeoObject(View view) {
        Intent intent = new Intent(MainMenuActivity.this, CreateRouteActivity.class);
        startActivity(intent);
        finish();
    }

    public void clickBtnViewGeoObjects(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_coordinates_changed", false);
        Intent intent = new Intent(MainMenuActivity.this, ViewGeoObjectsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
