package com.loop.touristroute;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class ThreadsClass {

    private Context myContext;
    final String APP_PREFERENCES = "app_settings";
    final String GEO_OBJECTS_BOUNDS = "geo_objects_bounds";
    SharedPreferences appSettings;

    public ThreadsClass(Context context) {
        myContext = context;
    }

    public LatLngBounds calculateBounds(String coordinates) {
        appSettings = myContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String stringBounds = appSettings.getString(GEO_OBJECTS_BOUNDS, "");
        ParserHelper ph = new ParserHelper();
        LatLng sw = ph.getSWFromBounds(stringBounds);
        LatLng ne = ph.getNEFromBounds(stringBounds);
        LatLng newCoordinates = ph.StringToLatLng(coordinates);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(sw);
        builder.include(ne);
        builder.include(newCoordinates);
        return builder.build();
    }

    public void calculateBounds() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                appSettings = myContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                String coordinates;
                ParserHelper ph = new ParserHelper();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                DBHelper dbHelper = new DBHelper(myContext);
                Cursor c = dbHelper.getAllCoordinates();
                if (c.moveToFirst()) {
                    do {
                        coordinates = c.getString(c.getColumnIndex("geo_object_coordinates"));
                        builder.include(ph.StringToLatLng(coordinates));
                    } while (c.moveToNext());
                }
                LatLngBounds bounds = builder.build();
                SharedPreferences.Editor editor = appSettings.edit();
                editor.putString(GEO_OBJECTS_BOUNDS, String.valueOf(bounds));
                editor.apply();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

}
