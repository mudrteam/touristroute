package com.loop.touristroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;

public class ViewRouteOnMapActivity extends AppCompatActivity
    implements OnMapReadyCallback{

    MapFragment mapFragment;
    GoogleMap map;
    String stringPolylineOptions;
    GoogleMapsHelper gmh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_route_on_map);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            stringPolylineOptions = bundle.getString("polyline_options");
        }
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap;
        gmh = new GoogleMapsHelper(this, map);
        gmh.setBounds("null");
        gmh.viewAllGeoObjects(-1);
        map.getUiSettings().setZoomControlsEnabled(true);
        ParserHelper ph = new ParserHelper();
        PolylineOptions po = ph.getPolylineOptionsFromString(stringPolylineOptions);
        if (po != null) {
            map.addPolyline(po);
        }
    }

    public void btnClickBack(View view) throws InterruptedException, ExecutionException, JSONException {
        Intent intent = new Intent(ViewRouteOnMapActivity.this, CreateRouteActivity.class);
        startActivity(intent);
        finish();
        /*
        GoogleMapsApiConnection gmh = new GoogleMapsApiConnection("(52.03319,38.727197)", "(51.297461,24.066027)");
        PolylineOptions polylineOptions = gmh.getPolylineOptions();
        if (polylineOptions != null) {
            googleMap.addPolyline(polylineOptions);
        }
        int duration = gmh.getDuration();
        LatLngBounds bounds = gmh.getBounds();
        if (bounds != null) {
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
        }
        */
    }

    public void btnClickSave(View view) {
        Intent intent = new Intent(ViewRouteOnMapActivity.this, CreateRouteActivity.class);
        startActivity(intent);
        finish();
    }

}
