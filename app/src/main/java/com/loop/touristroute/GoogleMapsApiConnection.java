package com.loop.touristroute;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class GoogleMapsApiConnection {

    private final PolylineOptions polylineOptions;
    private final int duration;
    private final LatLngBounds bounds;
    private ArrayList<LatLng> LatLngArrayList = new ArrayList<>();

    GoogleMapsApiConnection(String sourceLatLng, String destLatLng)
            throws ExecutionException, InterruptedException, JSONException {
        String json = getJSON(sourceLatLng, destLatLng);
        if (json != null) {
            this.duration = makeDuration(json);
            this.bounds = makeBounds(json);
            this.LatLngArrayList = getLatLngFromJson(json);
            this.polylineOptions = makePolyline();
        } else {
            this.duration = 0;
            this.bounds = null;
            this.LatLngArrayList = null;
            this.polylineOptions = null;
        }
    }

    private String makeURL (String sourceLatLng, String destLatLng){
        ParserHelper ph = new ParserHelper();
        return "http://maps.googleapis.com/maps/api/directions/json?origin=" +
                ph.getLatLngForUrlRequest(sourceLatLng) + "&destination=" +
                ph.getLatLngForUrlRequest(destLatLng) + "&sensor=false&units=metric&mode=walking";
    }

    private String getJSON(String sourceLatLng, String destLatLng) {
        try {
            return new getDataFromURL().execute(makeURL(sourceLatLng, destLatLng)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private PolylineOptions makePolyline() throws JSONException {
        PolylineOptions polylineOptions = new PolylineOptions();
        ArrayList<LatLng> latLngArrayList = LatLngArrayList;
        for (int i = 0; i< latLngArrayList.size(); i++) {
            polylineOptions.add(latLngArrayList.get(i));
        }
        return polylineOptions;
    }

    private ArrayList<LatLng> getLatLngFromJson(String jsonResponse) throws  JSONException {
        ArrayList<LatLng> latLngArray = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(jsonResponse);
        JSONArray jsonRoutes = jsonObject.getJSONArray("routes");
        JSONObject jsonObjectLegs = jsonRoutes.getJSONObject(0);
        JSONArray jsonLegs = jsonObjectLegs.getJSONArray("legs");
        JSONObject jsonObjectSteps = jsonLegs.getJSONObject(0);
        JSONArray jsonSteps = jsonObjectSteps.getJSONArray("steps");
        int numOfSteps = jsonSteps.length();
        for (int i = 0; i < numOfSteps; i++) {
            JSONObject startLocation = jsonSteps.getJSONObject(i).getJSONObject("start_location");
            String startLat = startLocation.getString("lat");
            String startLng = startLocation.getString("lng");
            LatLng start = new LatLng(Double.parseDouble(startLat), Double.parseDouble(startLng));
            latLngArray.add(start);
            if (i == numOfSteps - 1) {
                JSONObject endLocation = jsonSteps.getJSONObject(i).getJSONObject("end_location");
                String endLat = endLocation.getString("lat");
                String endLng = endLocation.getString("lng");
                LatLng end = new LatLng(Double.parseDouble(endLat), Double.parseDouble(endLng));
                latLngArray.add(end);
            }
        }
        return latLngArray;
    }

    private LatLngBounds makeBounds(String jsonResponse) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonResponse);
        JSONArray jsonRoutes = jsonObject.getJSONArray("routes");
        JSONObject jsonObjectRoutes = jsonRoutes.getJSONObject(0);
        JSONObject boundsObject = jsonObjectRoutes.getJSONObject("bounds");
        JSONObject northeast = boundsObject.getJSONObject("northeast");
        String neLat = northeast.getString("lat");
        String neLng = northeast.getString("lng");
        LatLng ne = new LatLng(Double.parseDouble(neLat), Double.parseDouble(neLng));
        JSONObject southwest = boundsObject.getJSONObject("southwest");
        String swLat = southwest.getString("lat");
        String swLng = southwest.getString("lng");
        LatLng sw = new LatLng(Double.parseDouble(swLat), Double.parseDouble(swLng));
        return new LatLngBounds(sw, ne);
    }

    private int makeDuration(String jsonResponse) throws JSONException{
        JSONObject jsonObject = new JSONObject(jsonResponse);
        JSONArray jsonRoutes = jsonObject.getJSONArray("routes");
        JSONObject jsonObjectRoutes = jsonRoutes.getJSONObject(0);
        JSONArray jsonLegs = jsonObjectRoutes.getJSONArray("legs");
        JSONObject jsonObjectLegs = jsonLegs.getJSONObject(0);
        JSONObject jsonObjectDuration = jsonObjectLegs.getJSONObject("duration");
        return jsonObjectDuration.getInt("value");
    }

    public PolylineOptions getPolylineOptions() {
        return polylineOptions;
    }

    public LatLngBounds getBounds() {
        return bounds;
    }

    public int getDuration() {
        return duration;
    }

    public ArrayList<LatLng> getLatLngArrayList() {
        return LatLngArrayList;
    }

}
