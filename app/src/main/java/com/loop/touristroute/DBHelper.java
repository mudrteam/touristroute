package com.loop.touristroute;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper{

    private SQLiteOpenHelper _openHelper;

    // public static final String TEXT_TYPE = " TEXT";
    // User markers
    public static final String TABLE_GEO_OBJECTS = "geo_objects";
    public static final String COLUMN_GEO_OBJECT_ID = "geo_object_id";
    public static final String COLUMN_GEO_OBJECT_NAME = "geo_object_name";
    public static final String COLUMN_GEO_OBJECT_COORDINATES = "geo_object_coordinates";
    public static final String COLUMN_GEO_OBJECT_TYPE = "geo_object_type";
    public static final String COLUMN_GEO_OBJECT_TITLE = "geo_object_title";
    public static final String COLUMN_GEO_OBJECT_lINK = "geo_object_link";
    public static final String COLUMN_GEO_OBJECT_TIME = "geo_object_time";

    public static final String CREATE_TABLE_GEO_OBJECTS = "create table if not exists " +
            TABLE_GEO_OBJECTS +
            " (" +
            COLUMN_GEO_OBJECT_ID + " integer primary key autoincrement," +
            COLUMN_GEO_OBJECT_NAME + "," +
            COLUMN_GEO_OBJECT_COORDINATES + "," +
            COLUMN_GEO_OBJECT_TYPE + "," +
            COLUMN_GEO_OBJECT_TITLE + "," +
            COLUMN_GEO_OBJECT_lINK + "," +
            COLUMN_GEO_OBJECT_TIME +
            ");";

    public DBHelper(Context context) {
        _openHelper = new SimpleSQLLiteOpenHelper(context);
    }

    class SimpleSQLLiteOpenHelper extends SQLiteOpenHelper {
        SimpleSQLLiteOpenHelper(Context context) {
            super(context,"TouristRouteDB",null,1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_GEO_OBJECTS);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public void deleteRow(long id) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db != null) {
            db.delete(TABLE_GEO_OBJECTS, COLUMN_GEO_OBJECT_ID + " = " + id, null);
            db.close();
        }
    }

    public long addRow(String name, String coordinates, String type, String title, String link, String time) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_GEO_OBJECT_NAME, name);
        cv.put(COLUMN_GEO_OBJECT_COORDINATES, coordinates);
        cv.put(COLUMN_GEO_OBJECT_TYPE, type);
        cv.put(COLUMN_GEO_OBJECT_TITLE, title);
        cv.put(COLUMN_GEO_OBJECT_lINK, link);
        cv.put(COLUMN_GEO_OBJECT_TIME, time);
        return db.insert(TABLE_GEO_OBJECTS, null, cv);
    }

    public ContentValues getAllColsInRow(long id) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        ContentValues cv = new ContentValues();
        Cursor c = db.rawQuery("select " + COLUMN_GEO_OBJECT_NAME + ", " +
                        COLUMN_GEO_OBJECT_COORDINATES + ", " +
                        COLUMN_GEO_OBJECT_TYPE + ", " +
                        COLUMN_GEO_OBJECT_TITLE + ", " +
                        COLUMN_GEO_OBJECT_lINK + ", " +
                        COLUMN_GEO_OBJECT_TIME +
                        " from " + TABLE_GEO_OBJECTS +
                " where " + COLUMN_GEO_OBJECT_ID + " = ?", new String[] { String.valueOf(id) });
        if (c.moveToNext()) {
            cv.put("name", c.getString(0));
            cv.put("coordinates", c.getString(1));
            cv.put("type", c.getString(2));
            cv.put("title", c.getString(3));
            cv.put("link", c.getString(4));
            cv.put("time", c.getString(5));
        }
        c.close();
        db.close();
        return cv;
    }

    public Cursor getAllRows(int columnNum, boolean isAscending) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String COLUMN_NAME;
        switch (columnNum){
            case 0:
                COLUMN_NAME = COLUMN_GEO_OBJECT_ID;
                break;
            case 1:
                COLUMN_NAME = COLUMN_GEO_OBJECT_NAME;
                break;
            case 2:
                COLUMN_NAME = COLUMN_GEO_OBJECT_COORDINATES;
                break;
            case 3:
                COLUMN_NAME = COLUMN_GEO_OBJECT_TYPE;
                break;
            case 4:
                COLUMN_NAME = COLUMN_GEO_OBJECT_TITLE;
                break;
            case 5:
                COLUMN_NAME = COLUMN_GEO_OBJECT_lINK;
                break;
            case 6:
                COLUMN_NAME = COLUMN_GEO_OBJECT_TIME;
                break;
            default:
                COLUMN_NAME = COLUMN_GEO_OBJECT_ID;
                break;
        }
        String SORT;
        if (isAscending) {
            SORT = " ASC";
        }
        else {
            SORT = " DESC";
        }
        return db.query(TABLE_GEO_OBJECTS, null, null, null, null, null, COLUMN_NAME + SORT);
    }

    public Cursor getAllRows() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        return db.query(TABLE_GEO_OBJECTS, null, null, null, null, null, null);
    }

    public Cursor getAllCoordinates() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        return db.rawQuery("select " + COLUMN_GEO_OBJECT_COORDINATES + " from " + TABLE_GEO_OBJECTS, null);
    }

    public Cursor getGeoObjectByType(int type) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        return db.rawQuery("select " + COLUMN_GEO_OBJECT_ID + ", " + COLUMN_GEO_OBJECT_COORDINATES +
                " from " + TABLE_GEO_OBJECTS + " where " + COLUMN_GEO_OBJECT_TYPE +
                " = ?", new String[] { String.valueOf(type) });
    }

    public Cursor getTheseColumns(boolean isNeedId, boolean isNeedName, boolean isNeedCoordinates,
                                  boolean isNeedType, boolean isNeedTitle, boolean isNeedLink,
                                  boolean isNeedTime) {
        boolean isFirstColumn = true;
        String columns = "";
        if (isNeedId) {
            columns = COLUMN_GEO_OBJECT_ID;
            isFirstColumn = false;
        }
        if (isNeedName) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_NAME;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_NAME;
            }
            isFirstColumn = false;
        }
        if (isNeedCoordinates) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_COORDINATES;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_COORDINATES;
            }
            isFirstColumn = false;
        }
        if (isNeedType) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_TYPE;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_TYPE;
            }
            isFirstColumn = false;
        }
        if (isNeedTitle) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_TITLE;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_TITLE;
            }
            isFirstColumn = false;
        }
        if (isNeedLink) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_lINK;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_lINK;
            }
            isFirstColumn = false;
        }
        if (isNeedTime) {
            if (isFirstColumn) {
                columns = COLUMN_GEO_OBJECT_TIME;
            } else {
                columns = columns + ", " + COLUMN_GEO_OBJECT_TIME;
            }
        }
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        return db.rawQuery("select " + columns + " from " + TABLE_GEO_OBJECTS, null);
    }

    // "" - delete data from column; null - nothing to do with column
    public void editRow(long id, String name, String coordinates, String type, String title, String link, String time) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }
        ContentValues cv = new ContentValues();
        if (name != null) {
            cv.put(COLUMN_GEO_OBJECT_NAME, name);
        }
        if (coordinates != null) {
            cv.put(COLUMN_GEO_OBJECT_COORDINATES, coordinates);
        }
        if (type != null) {
            cv.put(COLUMN_GEO_OBJECT_TYPE, type);
        }
        if (title != null) {
            cv.put(COLUMN_GEO_OBJECT_TITLE, title);
        }
        if (link != null) {
            cv.put(COLUMN_GEO_OBJECT_lINK, link);
        }
        if (time != null) {
            cv.put(COLUMN_GEO_OBJECT_TIME, time);
        }
        db.update(TABLE_GEO_OBJECTS, cv, COLUMN_GEO_OBJECT_ID + " = ?", new String[] {String.valueOf(id)});
        db.close();
    }

    public void close() {
        _openHelper.close();
    }

}
