package com.loop.touristroute;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class EditGeoObjectParametersActivity extends AppCompatActivity
        implements View.OnFocusChangeListener, View.OnClickListener{

    EditText etGeoObjectName, etGeoObjectTitle, etGeoObjectLink, etGeoObjectTimeHours,
            etGeoObjectTimeMinutes;
    Button btnSetCoordinates;
    Spinner spinnerGeoObjectType;

    private String geoObjectCoordinates = "null";
    private String geoObjectName = "";
    private int geoObjectType = 0;
    private String geoObjectTitle = "";
    private String geoObjectLink = "";
    private String geoObjectTime = "00:00";
    private long geoObjectId = -1;
    private String notSavedBounds = "null";

    private String colorRed = "#ff9a9a";
    private String colorGreen = "#9aff9a";
    private String colorBrown = "#ffb764";
    private String colorWhite = "#ffffff";

    private static final int btnCancelId = View.generateViewId();
    private static final int btnDeleteId = View.generateViewId();
    private static final int btnApplyId = View.generateViewId();
    private static final int btnAddId = View.generateViewId();

    String[] types = {"eatery", "museum", "attraction", "amusement","hotel"};

    StringValidator sv = new StringValidator();
    ParserHelper ph = new ParserHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_edit_geo_object_parameters);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        readBundle();
    }

    private void readBundle() {
        Bundle bundle = getIntent().getExtras();
        btnSetCoordinates = (Button) findViewById(R.id.btnSetCoordinates);
        if (bundle != null) {
            String bundleCoordinates = bundle.getString("geo_object_coordinates");
            String bundleName = bundle.getString("geo_object_name");
            String bundleTitle = bundle.getString("geo_object_title");
            String bundleLink = bundle.getString("geo_object_link");
            String bundleTime = bundle.getString("geo_object_time");
            boolean isCoordinatesChanged = bundle.getBoolean("is_coordinates_changed");
            if (bundleCoordinates != null) {
                geoObjectCoordinates = bundleCoordinates;
                if (geoObjectId != -1) {
                    if (geoObjectCoordinates.equals("null")) {
                        btnSetCoordinates.setBackgroundColor(Color.parseColor(colorRed));
                    } else {
                        btnSetCoordinates.setBackgroundColor(Color.parseColor(colorGreen));
                    }
                }
            }
            if (bundleName != null) {
                geoObjectName = bundleName;
            }
            if (bundleTitle != null) {
                geoObjectTitle = bundleTitle;
            }
            if (bundleLink != null) {
                geoObjectLink = bundleLink;
            }
            if (bundleTime != null) {
                geoObjectTime = bundleTime;
            }
            geoObjectType = bundle.getInt("geo_object_type");
            geoObjectId = bundle.getLong("geo_object_id");
            if (isCoordinatesChanged) {
                if (!geoObjectCoordinates.equals("null")) {
                    ThreadsClass th = new ThreadsClass(this);
                    notSavedBounds = String.valueOf(th.calculateBounds(geoObjectCoordinates));
                }
            }
        }
        //              All EditText
        etGeoObjectName = (EditText) findViewById(R.id.editTextGeoObjectName);
        etGeoObjectTitle = (EditText) findViewById(R.id.editTextGeoObjectTitle);
        etGeoObjectLink = (EditText) findViewById(R.id.editTextGeoObjectLink);
        etGeoObjectTimeHours = (EditText) findViewById(R.id.editTextHours);
        etGeoObjectTimeMinutes = (EditText) findViewById(R.id.editTextMinutes);
        etGeoObjectName.setText(geoObjectName);
        etGeoObjectTitle.setText(geoObjectTitle);
        etGeoObjectLink.setText(geoObjectLink);
        etGeoObjectTimeHours.setText(ph.getHoursFromTime(geoObjectTime));
        etGeoObjectTimeMinutes.setText(ph.getMinutesFromTime(geoObjectTime));
        etGeoObjectName.setHint("object name");
        etGeoObjectTitle.setHint("some information about object");
        etGeoObjectLink.setHint("https://example.com");
        etGeoObjectName.setOnFocusChangeListener(this);
        etGeoObjectTitle.setOnFocusChangeListener(this);
        etGeoObjectLink.setOnFocusChangeListener(this);
        etGeoObjectTimeHours.setFilters(new InputFilter[]{new EditTextInputFilter(0, 12)});
        etGeoObjectTimeMinutes.setFilters(new InputFilter[]{new EditTextInputFilter(0, 59)});
        //          Spinner "geo object type"
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.spinner_item_layout, types);
        spinnerGeoObjectType = (Spinner) findViewById(R.id.spinnerGeoObjectType);
        spinnerGeoObjectType.setAdapter(adapter);
        spinnerGeoObjectType.setSelection(geoObjectType);
        addControlButtons(geoObjectId);
    }

    private void addControlButtons(long id) {
        LayoutHelper lh = new LayoutHelper();
        LinearLayout layout_btns = (LinearLayout)findViewById(R.id.btnsLayout);
        LinearLayout.LayoutParams layoutParams = lh.getControlBtnViewParams();
        Button btnCancel = new Button(this);
        btnCancel.setId(btnCancelId);
        btnCancel.setText("Cancel");
        btnCancel.setOnClickListener(this);
        btnCancel.setLayoutParams(layoutParams);
        layout_btns.addView(btnCancel);
        if (id != -1) {
            Button btnDelete = new Button(this);
            btnDelete.setId(btnDeleteId);
            btnDelete.setText("Delete");
            btnDelete.setOnClickListener(this);
            btnDelete.setLayoutParams(layoutParams);
            layout_btns.addView(btnDelete);
            Button btnApply = new Button(this);
            btnApply.setId(btnApplyId);
            btnApply.setText("Apply");
            btnApply.setOnClickListener(this);
            btnApply.setLayoutParams(layoutParams);
            layout_btns.addView(btnApply);
        }
        else {
            Button btnAdd = new Button(this);
            btnAdd.setId(btnAddId);
            btnAdd.setText("Add");
            btnAdd.setOnClickListener(this);
            btnAdd.setLayoutParams(layoutParams);
            layout_btns.addView(btnAdd);
        }
    }

    public void onClick(View view) {
        final int clicked_id = view.getId();
        if (clicked_id == btnCancelId) {
            clickBtnCancel();
        }
        else if (clicked_id == btnDeleteId) {
            clickBtnDelete();
        }
        else if (clicked_id == btnApplyId) {
            clickBtnApply();
        }
        else if (clicked_id == btnAddId) {
            clickBtnAdd();
        }
    }

    private void getGeoObjectParameters() {
        geoObjectName = String.valueOf(etGeoObjectName.getText());
        geoObjectName = sv.removeWhitespaces(geoObjectName);
        geoObjectTitle = String.valueOf(etGeoObjectTitle.getText());
        geoObjectTitle = sv.removeWhitespaces(geoObjectTitle);
        geoObjectLink = String.valueOf(etGeoObjectLink.getText());
        geoObjectLink = sv.removeWhitespaces(geoObjectLink);
        geoObjectTime = ph.twoNumbersToTime(String.valueOf(etGeoObjectTimeHours.getText()),
                String.valueOf(etGeoObjectTimeMinutes.getText()));
        geoObjectType = spinnerGeoObjectType.getSelectedItemPosition();
    }

    private boolean checkEditText () {
        boolean isCorrectStrings = true;
        getGeoObjectParameters();
        //              Check name
        if (sv.isEmptyString(geoObjectName)) {
            etGeoObjectName.setBackgroundColor(Color.parseColor(colorRed));
            isCorrectStrings = false;
        } else {
            etGeoObjectName.setBackgroundColor(Color.parseColor(colorGreen));
        }
        //              check title
        if (sv.isEmptyString(geoObjectTitle)) {
            etGeoObjectTitle.setBackgroundColor(Color.parseColor(colorWhite));
        } else {
            etGeoObjectTitle.setBackgroundColor(Color.parseColor(colorGreen));
        }
        //              Check link
        if (sv.isEmptyString(geoObjectLink)) {
            etGeoObjectLink.setBackgroundColor(Color.parseColor(colorWhite));
        } else if (!sv.isValidURL(geoObjectLink)){
            etGeoObjectLink.setBackgroundColor(Color.parseColor(colorBrown));
            isCorrectStrings = false;
        } else {
            etGeoObjectLink.setBackgroundColor(Color.parseColor(colorGreen));
        }
        //              Check coordinates
        if (geoObjectCoordinates.equals("null")) {
            btnSetCoordinates.setBackgroundColor(Color.parseColor(colorRed));
            isCorrectStrings = false;
        } else {
            btnSetCoordinates.setBackgroundColor(Color.parseColor(colorGreen));
        }
        return isCorrectStrings;
    }

    public void clickBtnSetCoordinates(View view) {
        getGeoObjectParameters();
        Bundle bundle = new Bundle();
        Intent intent = new Intent(EditGeoObjectParametersActivity.this,
                EditGeoObjectCoordinatesActivity.class);
        bundle.putString("geo_object_coordinates", geoObjectCoordinates);
        bundle.putString("geo_object_name", geoObjectName);
        bundle.putString("geo_object_title", geoObjectTitle);
        bundle.putString("geo_object_link", geoObjectLink);
        bundle.putString("geo_object_time", geoObjectTime);
        bundle.putInt("geo_object_type", geoObjectType);
        bundle.putLong("geo_object_id", geoObjectId);
        bundle.putString("lat_lng_bounds", notSavedBounds);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void clickBtnCancel() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_coordinates_changed", false);
        Intent intent = new Intent(EditGeoObjectParametersActivity.this, ViewGeoObjectsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void clickBtnApply() {
        if (checkEditText()) {
            DBHelper dbHelper = new DBHelper(this);
            // is coordinates changed?
            ContentValues cv = dbHelper.getAllColsInRow(geoObjectId);
            String previousCoordinates = cv.getAsString("coordinates");
            cv.clear();
            if (previousCoordinates == null) {
                previousCoordinates = "null";
            }
            boolean isChanged = !previousCoordinates.equals(geoObjectCoordinates);
            //
            Bundle bundle = new Bundle();
            bundle.putBoolean("is_coordinates_changed", isChanged);
            dbHelper.editRow(geoObjectId, geoObjectName, geoObjectCoordinates,
                    String.valueOf(geoObjectType), geoObjectTitle, geoObjectLink, geoObjectTime);
            dbHelper.close();
            Intent intent = new Intent(EditGeoObjectParametersActivity.this, ViewGeoObjectsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
    }

    private void clickBtnDelete() {
        if (geoObjectId != -1) {
            DBHelper dbHelper = new DBHelper(this);
            dbHelper.deleteRow(geoObjectId);
            dbHelper.close();
            Bundle bundle = new Bundle();
            bundle.putBoolean("is_coordinates_changed", true);
            Intent intent = new Intent(EditGeoObjectParametersActivity.this, ViewGeoObjectsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
    }

    private void clickBtnAdd() {
        if (checkEditText()) {
            DBHelper dbHelper = new DBHelper(this);
            dbHelper.addRow(geoObjectName, geoObjectCoordinates, String.valueOf(geoObjectType),
                    geoObjectTitle, geoObjectLink, geoObjectTime);
            dbHelper.close();
            Bundle bundle = new Bundle();
            bundle.putBoolean("is_coordinates_changed", true);
            Intent intent = new Intent(EditGeoObjectParametersActivity.this, ViewGeoObjectsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
    }

    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()){
            case R.id.editTextGeoObjectName:
                if (!hasFocus) {
                    geoObjectName = String.valueOf(etGeoObjectName.getText());
                    geoObjectName = sv.removeWhitespaces(geoObjectName);
                    etGeoObjectName.setText(geoObjectName);
                    if (sv.isEmptyString(geoObjectName)) {
                        etGeoObjectName.setBackgroundColor(Color.parseColor(colorRed));
                    } else {
                        etGeoObjectName.setBackgroundColor(Color.parseColor(colorGreen));
                    }
                }
                break;
            case R.id.editTextGeoObjectTitle:
                if (!hasFocus) {
                    geoObjectTitle = String.valueOf(etGeoObjectTitle.getText());
                    geoObjectTitle = sv.removeWhitespaces(geoObjectTitle);
                    etGeoObjectTitle.setText(geoObjectTitle);
                    if (sv.isEmptyString(geoObjectTitle)) {
                        etGeoObjectTitle.setBackgroundColor(Color.parseColor(colorWhite));
                    } else {
                        etGeoObjectTitle.setBackgroundColor(Color.parseColor(colorGreen));
                    }
                }
                break;
            case R.id.editTextGeoObjectLink:
                if (!hasFocus) {
                    geoObjectLink = String.valueOf(etGeoObjectLink.getText());
                    geoObjectLink = sv.removeWhitespaces(geoObjectLink);
                    etGeoObjectLink.setText(geoObjectLink);
                    if (sv.isEmptyString(geoObjectLink)) {
                        etGeoObjectLink.setBackgroundColor(Color.parseColor(colorWhite));
                    } else if (!sv.isValidURL(geoObjectLink)){
                        etGeoObjectLink.setBackgroundColor(Color.parseColor(colorBrown));
                    } else {
                        etGeoObjectLink.setBackgroundColor(Color.parseColor(colorGreen));
                    }
                }
                break;
        }
    }

}
