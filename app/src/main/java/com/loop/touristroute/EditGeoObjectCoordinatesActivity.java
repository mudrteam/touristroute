package com.loop.touristroute;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;


public class EditGeoObjectCoordinatesActivity extends Activity
        implements OnMapReadyCallback, OnMarkerDragListener, OnMarkerClickListener {


    int padding = 60;

    MapFragment mapFragment;
    GoogleMap map;

    LatLng markerLatLng = null;
    String geoObjectCoordinates = "null";
    String notSavedBounds = "null";
    String geoObjectName = "";
    String geoObjectTitle = "";
    String geoObjectLink = "";
    String geoObjectTime = "";
    int geoObjectType = 0;
    long geoObjectId = -1;
    boolean isCoordinatesChanged = false;

    GoogleMapsHelper gmh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_edit_geo_object_coordinates);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox_view_all_objects);
        checkBox.setChecked(true);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    gmh.viewAllGeoObjects(geoObjectId);
                } else {
                    removeGeoObjects();
                }
            }
        });
        getDataFromBundle();
    }

    private void getDataFromBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            geoObjectCoordinates = bundle.getString("geo_object_coordinates");
            if (geoObjectCoordinates != null) {
                if (!geoObjectCoordinates.equals("null")) {
                    ParserHelper ph = new ParserHelper();
                    markerLatLng = ph.StringToLatLng(geoObjectCoordinates);
                }
            }
            geoObjectName = bundle.getString("geo_object_name");
            geoObjectTitle = bundle.getString("geo_object_title");
            geoObjectLink = bundle.getString("geo_object_link");
            geoObjectTime = bundle.getString("geo_object_time");
            geoObjectType = bundle.getInt("geo_object_type");
            geoObjectId = bundle.getLong("geo_object_id");
            notSavedBounds = bundle.getString("lat_lng_bounds");
        }
    }

    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap;
        gmh = new GoogleMapsHelper(this, map);
        gmh.viewAllGeoObjects(geoObjectId);
        gmh.setBounds(notSavedBounds);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMarkerDragListener(this);
        map.setOnMapClickListener(new OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (markerLatLng == null) {
                    Marker marker = map.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .title(geoObjectName)
                                    .draggable(true)
                                    .snippet(geoObjectTitle)
                    );
                    gmh.setMarkerIcon(marker, geoObjectType);
                    markerLatLng = latLng;
                    isCoordinatesChanged = true;
                }
            }
        });

        if (markerLatLng != null) {
            Marker marker = map.addMarker(new MarkerOptions()
                            .position(markerLatLng)
                            .title(geoObjectName)
                            .draggable(true)
                            .snippet(geoObjectTitle)
            );
            gmh.setMarkerIcon(marker, geoObjectType);
        }
    }

    public boolean onMarkerClick(final Marker marker) {
        return true;
    }

    public void onMarkerDragEnd(final Marker marker) {
        markerLatLng = marker.getPosition();
        isCoordinatesChanged = true;
    }

    public void onMarkerDrag(final Marker marker) {

    }

    public void onMarkerDragStart(final Marker marker) {

    }

    private Bundle packBundle(boolean isSave) {
        Bundle bundle = new Bundle();
        if (isSave) {
            if (markerLatLng != null) {
                ParserHelper ph = new ParserHelper();
                bundle.putString("geo_object_coordinates", ph.LatLngToString(markerLatLng));
            } else {
                bundle.putString("geo_object_coordinates", "null");
            }
            bundle.putBoolean("is_coordinates_changed", isCoordinatesChanged);
        }
        else {
            if (!geoObjectCoordinates.equals("null")) {
                bundle.putString("geo_object_coordinates", geoObjectCoordinates);
            } else {
                bundle.putString("geo_object_coordinates", "null");
            }
            bundle.putBoolean("is_coordinates_changed", false);
        }
        bundle.putString("geo_object_name", geoObjectName);
        bundle.putString("geo_object_title", geoObjectTitle);
        bundle.putString("geo_object_link", geoObjectLink);
        bundle.putString("geo_object_time", geoObjectTime);
        bundle.putInt("geo_object_type", geoObjectType);
        bundle.putLong("geo_object_id", geoObjectId);
        return bundle;
    }

    public void removeGeoObjects() {
        map.clear();
        if (markerLatLng != null) {
            Marker marker = map.addMarker(new MarkerOptions()
                            .position(markerLatLng)
                            .title(geoObjectName)
                            .draggable(true)
                            .snippet(geoObjectTitle)
            );
            gmh.setMarkerIcon(marker, geoObjectType);
        }
    }

    public void btnClickBack(View view) throws InterruptedException, ExecutionException, JSONException {

        Intent intent = new Intent(EditGeoObjectCoordinatesActivity.this, EditGeoObjectParametersActivity.class);
        intent.putExtras(packBundle(false));
        startActivity(intent);
        finish();
    }

    public void btnClickSave(View view) {
        Intent intent = new Intent(EditGeoObjectCoordinatesActivity.this, EditGeoObjectParametersActivity.class);
        intent.putExtras(packBundle(true));
        startActivity(intent);
        finish();
    }

}
